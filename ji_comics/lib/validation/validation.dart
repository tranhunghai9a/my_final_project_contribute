
mixin CommonValidation {
  String? validateEmail(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (!value.contains('.')) {
      return 'It must contain dot';
    }
    if (!value.contains('@')) {
      return 'It must contain @';
    }

    return null;
  }

  String? validatePassword(String? value) {
    if (value!.isEmpty) {
      return 'Please fill the blank';
    }
    if (value.length != 8) {
      return 'At least 8 characters long';
    }

    return null;
  }

}