import 'package:flutter/material.dart';
import '../views/register.dart';
import '../validation/validation.dart';
import '../theme/colors.dart';


class LogIn extends StatefulWidget {
  static const route= '/logIn';
  @override
  State<StatefulWidget> createState() {
    return LogInState();
  }

}

class LogInState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Jii Comics App'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: jii_color
          ),
        )),
      body: LogInField(),
    );
  }

}
class LogInField extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LogInFieldState();
  }

}

class LogInFieldState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Welcome To Jii Comics",
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 30,
                    color: primary_one),),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldEmail(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldPassword(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              loginButton(),
              Container(margin: EdgeInsets.only(top: 10.0),),
              Text("By signing up, you agree to Jii Comic’s Terms of Service and Privacy Policy."),
              Container(margin: EdgeInsets.only(top: 30.0),),
              registerButton()
            ],
          ),
        )

    );
  }

  Widget fieldEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email),
          labelText: 'example@gmail.com',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          )
      ),
      validator: validateEmail,
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'password',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          )
      ),
      validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
    );
  }




  Widget loginButton() {
    return ElevatedButton(
      onPressed: () {
        if (formKey.currentState!.validate()) {
          // Call API Authentication from Backend
          formKey.currentState!.save();
          //Navigator.of(context).pushReplacementNamed(???.route);
        }
      },
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20))),
      child: Ink(
        decoration: BoxDecoration(
            gradient:
            jii_color,
            borderRadius: BorderRadius.circular(20)),
        child: Container(
          width: 100,
          height: 50,
          alignment: Alignment.center,
          child: const Text(
            'Login',
          ),
        ),
      ),
    );
  }

  Widget registerButton() {
    return ElevatedButton(
      onPressed: () {
          Navigator.of(context).pushReplacementNamed(Register.route);
        }
      ,
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20))),
      child: Ink(
        decoration: BoxDecoration(
            gradient:
            jii_color,
            borderRadius: BorderRadius.circular(20)),
        child: Container(
          width: 100,
          height: 50,
          alignment: Alignment.center,
          child: const Text(
            'Register',
          ),
        ),
      ),
    );
  }

}