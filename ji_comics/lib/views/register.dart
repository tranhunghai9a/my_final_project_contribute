import 'package:flutter/material.dart';
import '../views/log_in.dart';
import '../validation/validation.dart';
import '../theme/colors.dart';


class Register extends StatefulWidget {
  static const route= '/register';
  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }

}

class RegisterState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Jii Comics App'),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: jii_color
            ),
          )),
      body: RegisterField(),
    );
  }

}
class RegisterField extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterFieldState();
  }

}

class RegisterFieldState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Let's Create Your Account!",
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 30,
                    color: primary_one),),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldEmail(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldPassword(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              registerButton()
            ],
          ),
        )

    );
  }

  Widget fieldEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email),
          labelText: 'example@gmail.com',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          )
      ),
      validator: validateEmail,
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'password',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          )
      ),
      validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
    );
  }




  Widget registerButton() {
    return ElevatedButton(
      onPressed: () {
        Navigator.of(context).pushReplacementNamed(LogIn.route);
      }
      ,
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20))),
      child: Ink(
        decoration: BoxDecoration(
            gradient:
            jii_color,
            borderRadius: BorderRadius.circular(20)),
        child: Container(
          width: 100,
          height: 50,
          alignment: Alignment.center,
          child: const Text(
            'Register',
          ),
        ),
      ),
    );
  }

}