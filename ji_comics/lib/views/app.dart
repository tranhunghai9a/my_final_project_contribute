import 'package:flutter/material.dart';
import './log_in.dart';
import './register.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => LogIn(),
        '/logIn': (context) => LogIn(),
        '/register': (context) => Register()
      },
      initialRoute: '/',
    );
  }

}
